﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Entities
{
    public class Book
    {
        public int Id { get; set; }

        [Required]
        [MaxLength(225)]
        public string Title { get; set; }

        [Required]
        [MaxLength(50)]
        public string Author { get; set; }

        [Required]
        public int PublishedYear { get; set; }

        [Required]
        [MaxLength(20)]
        public string IsAvailable { get; set; }
    }
}
