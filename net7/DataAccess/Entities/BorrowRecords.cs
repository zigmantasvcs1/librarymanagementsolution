﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Entities
{
    public class BorrowRecords
    {
        public int Id { get; set; }

        [Required]
        [ForeignKey("Books1Id")]
        public int Books1Id { get; set; }
        public Book? Books1 { get; private set; }

        [Required]
        [ForeignKey("ReadersId")]
        public int ReadersId { get; set; }
        public Reader? Readers { get; private set; }

        [Required]
        [MaxLength(20)]
        public DateTime BorrowDate { get; set; }

        [Required]
        [MaxLength(20)]
        public DateTime ReturnDate { get; set; }
    }
}
