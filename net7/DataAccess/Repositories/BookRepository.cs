﻿using DataAccess.Entities;
using Microsoft.EntityFrameworkCore;

namespace DataAccess.Repositories
{
    public class BookRepository
    {
        private readonly LibraryDbContext _context;

        public BookRepository(LibraryDbContext context)
        {
            _context = context;
        }

        //Create metodas
        public async Task CreateAsync(Book book)
        {
            _context.Books1.Add(book);
            await _context.SaveChangesAsync();
        }

        //Read metodas
        public async Task<Book?> ReadAsync(int id)
        {
            return await _context
                .Books1
                .SingleOrDefaultAsync(x => x.Id == id);
        }

        public async Task<List<Book>> ReadAsync()
        {
            return await _context.Books1.ToListAsync();
        }

        //Update metodas
        public async Task UpdateAsync(Book book)
        {
            var existingBook = await _context.Books1.FindAsync(book.Id);

            if (existingBook == null)
            {
                throw new KeyNotFoundException($"Book with ID {book.Id} not found.");
            }

            _context.Entry(existingBook).CurrentValues.SetValues(book);

            await _context.SaveChangesAsync();
        }

        // Delete metodas
        public async Task DeleteAsync(int id)
        {
            var book = await ReadAsync(id);

            if (book != null)
            {
                _context.Books1.Remove(book);
                await _context.SaveChangesAsync();
            }
        }
    }
}
