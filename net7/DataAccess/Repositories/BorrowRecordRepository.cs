﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccess.Entities;
using Microsoft.EntityFrameworkCore;

namespace DataAccess.Repositories
{
    public class BorrowRecordRepository
    {
        private readonly LibraryDbContext _context;

        public BorrowRecordRepository(LibraryDbContext context)
        {
            _context = context;
        }

        //Create metodas
        public async Task CreateAsync(BorrowRecords borrowRecords)
        {
            _context.BorrowRecords.Add(borrowRecords);
            await _context.SaveChangesAsync();
        }

        //Read metodas
        public async Task<BorrowRecords?> ReadAsync(int id)
        {
            return await _context.BorrowRecords
                .Include(brr => brr.Books1)
                .Include(brr => brr.Readers)
                .SingleOrDefaultAsync(x => x.Id == id);
        }

        public async Task<List<BorrowRecords>> ReadAsync()
        {
            return await _context.BorrowRecords
                .Include(brr => brr.Books1)
                .Include(brr => brr.Readers)
                .ToListAsync();
        }

        //Update metodas
        public async Task UpdateAsync(BorrowRecords borrowRecord)
        {
            var existingBorrowRecord = await _context.Readers.FindAsync(borrowRecord.Id);

            if (existingBorrowRecord == null)
            {
                throw new KeyNotFoundException($"Borrow record with ID {borrowRecord.Id} not found.");
            }

            _context.Entry(existingBorrowRecord).CurrentValues.SetValues(borrowRecord);

            await _context.SaveChangesAsync();
        }

        // Delete metodas
        public async Task DeleteAsync(int id)
        {
            var borrowRecord = await ReadAsync(id);

            if (borrowRecord != null)
            {
                _context.BorrowRecords.Remove(borrowRecord);
                await _context.SaveChangesAsync();
            }
        }
    }
}
