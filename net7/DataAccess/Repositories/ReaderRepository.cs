﻿using DataAccess.Entities;
using Microsoft.EntityFrameworkCore;

namespace DataAccess.Repositories
{
    public class ReaderRepository
    {
        private readonly LibraryDbContext _context;

        public ReaderRepository(LibraryDbContext context)
        {
            _context = context;
        }

        //Create metodas
        public async Task CreateAsync(Reader reader)
        {
            _context.Readers.Add(reader);
            await _context.SaveChangesAsync();
        }

        //Read metodas
        public async Task<Reader?> ReadAsync(int id)
        {
            return await _context
                .Readers
                .SingleOrDefaultAsync(x => x.Id == id);
        }

        public async Task<List<Reader>> ReadAsync()
        {
            return await _context.Readers.ToListAsync();
        }

        //Update metodas
        public async Task UpdateAsync(Reader reader)
        {
            var existingReader = await _context.Readers.FindAsync(reader.Id);

            if (existingReader == null)
            {
                throw new KeyNotFoundException($"Reader with ID {reader.Id} not found.");
            }

            _context.Entry(existingReader).CurrentValues.SetValues(reader);

            await _context.SaveChangesAsync();
        }

        // Delete metodas
        public async Task DeleteAsync(int id)
        {
            var reader = await ReadAsync(id);

            if (reader != null)
            {
                _context.Readers.Remove(reader);
                await _context.SaveChangesAsync();
            }
        }
    }
}
