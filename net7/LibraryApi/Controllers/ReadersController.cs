﻿using DataAccess.Entities;
using DataAccess.Repositories;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using task0241.ConsoleApp;

namespace LibraryApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ReadersController : ControllerBase
    {
        [HttpPost]
        public async Task<IActionResult> CreateReader(Reader reader)
        {
            var dbContextFactory = new LibraryDbContextFactory();

            try
            {
                using var context = dbContextFactory.CreateDbContext(new string[] { });
                var readerRepository = new ReaderRepository(context);

                await readerRepository.CreateAsync(reader);

                return Ok(reader);
            }
            catch (Exception ex)
            {
                System.IO.File.AppendAllText("Errors.txt", ex.ToString());

                return BadRequest("Serviso klaida.");
            }
            finally
            {
                System.IO.File.AppendAllText("logs.txt", "\r\n" + DateTime.Now.ToString() + " Pabandyta įvesti naują skaitytoją. ");
            }
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetReader(int id)
        {
            var dbContextFactory = new LibraryDbContextFactory();

            try
            {
                using var context = dbContextFactory.CreateDbContext(new string[] { });
                var readerRepository = new ReaderRepository(context);

                var reader = await readerRepository.ReadAsync(id);
                if (reader == null)
                {
                    throw new Exception("Tokio skaitytojo duomenų bazėje nėra.");
                }

                return Ok(reader);
            }
            catch (Exception ex)
            {
                System.IO.File.AppendAllText("Errors.txt", ex.ToString());

                return BadRequest("Serviso klaida.");
            }
            finally
            {
                System.IO.File.AppendAllText("logs.txt", "\r\n" + DateTime.Now.ToString() + $" Atliktas skaitytojo nr.{id} nuskaitymas. ");
            }
        }

        [HttpGet]
        public async Task<IActionResult> GetReader()
        {
            var dbContextFactory = new LibraryDbContextFactory();

            try
            {
                using var context = dbContextFactory.CreateDbContext(new string[] { });
                var readerRepository = new ReaderRepository(context);

                var readers = await readerRepository.ReadAsync();
                return Ok(readers);
            }
            catch (Exception ex)
            {
                System.IO.File.AppendAllText("Errors.txt", ex.ToString());

                return BadRequest("Servisos klaida.");
            }
            finally
            {
                System.IO.File.AppendAllText("logs.txt", "\r\n" + DateTime.Now.ToString() + $" Atliktas visų skaitytojų nuskaitymas. ");
            }
        }
    }
}
