﻿using DataAccess.Entities;
using DataAccess.Repositories;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using task0241.ConsoleApp;

namespace LibraryApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BooksController : ControllerBase
    {
        [HttpPost]
        public async Task<IActionResult> CreateBook(Book book) // Asinchroninis metodas naujos knygos kūrimui
        {
            var dbContextFactory = new LibraryDbContextFactory(); // Sukuriamas DbContextFactory objektas

            try
            {
                using var context = dbContextFactory.CreateDbContext(new string[] { });
                var bookRepository = new BookRepository(context); // Sukuriamas BookRepository objektas su sukurtu kontekstu

                await bookRepository.CreateAsync(book); // Asinchroniškai pridedamas kursas į duomenų bazę naudojant repozitoriją
                                                        // Grąžinamas HTTP atsakymas su sukurtu kursu
                return Ok(book);
            }
            catch (Exception ex)
            {
                System.IO.File.AppendAllText("Errors.txt", ex.ToString());

                return BadRequest("Serviso klaida.");
            }
            finally
            {
                System.IO.File.AppendAllText("logs.txt", "\r\n" + DateTime.Now.ToString() + " Pabandyta įvesti naują knygą. ");
            }
        }
        
        [HttpGet("{id}")]
        public async Task<IActionResult> GetBook(int id)
        {
            var dbContextFactory = new LibraryDbContextFactory(); // Sukuriamas DbContextFactory objektas
            
            try
            {
                using var context = dbContextFactory.CreateDbContext(new string[] { });
                var bookRepository = new BookRepository(context); // Sukuriamas BookRepository objektas su kontekstu

                var book = await bookRepository.ReadAsync(id); // Gaunama ieškoma knyga iš duomenų bazės

                if (book == null)
                {
                    throw new Exception("Jūsų pasirinktos knygos nėra.");
                    //return NotFound(); - kai atsirado exception handling - šito nebereikia.
                }

                return Ok(book);
            }
            catch (Exception ex)
            {
                System.IO.File.AppendAllText("Errors.txt", ex.ToString());

                return BadRequest("Serviso klaida.");
            }
            finally
            {
                System.IO.File.AppendAllText("logs.txt", "\r\n" + DateTime.Now.ToString() + $" Atliktas knygos nr.{id} nuskaitymas. ");
            }
        }

        [HttpGet]
        public async Task<IActionResult> GetBook()
        {
            var dbContextFactory = new LibraryDbContextFactory(); // Sukuriamas DbContextFactory objektas

            try
            {
                using var context = dbContextFactory.CreateDbContext(new string[] { });
                var bookRepository = new BookRepository(context); // Sukuriamas BookRepository objektas su kontekstu

                var books = await bookRepository.ReadAsync(); // Gaunamos visos knygos iš duomenų bazės
                return Ok(books);
            }
            catch (Exception ex)
            {
                System.IO.File.AppendAllText("Errors.txt", ex.ToString());

                return BadRequest("Serviso klaida.");
            }
            finally
            {
                System.IO.File.AppendAllText("logs.txt", "\r\n" + DateTime.Now.ToString() + $" Atliktas knygų bibliotekos nuskaitymas. ");
            }
        }
    }
}
