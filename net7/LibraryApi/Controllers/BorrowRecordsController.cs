﻿using DataAccess.Entities;
using DataAccess.Repositories;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using task0241.ConsoleApp;

namespace LibraryApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BorrowRecordsController : ControllerBase
    {
        [HttpPost]
        public async Task<IActionResult> CreateBorrowRecord(BorrowRecords borrowRecord)
        {
            var dbContextFactory = new LibraryDbContextFactory();

            try
            {
                using var context = dbContextFactory.CreateDbContext(new string[] { });
                var borrowRecordRepository = new BorrowRecordRepository(context);

                await borrowRecordRepository.CreateAsync(borrowRecord);
                return Ok(borrowRecord);
            }
            catch (Exception ex)
            {
                System.IO.File.AppendAllText("Errors.txt", ex.ToString());

                return BadRequest("Serviso klaida.");
            }
            finally
            {
                System.IO.File.AppendAllText("logs.txt", "\r\n" + DateTime.Now.ToString() + " Pabandyta įvesti naują skolinimosi įrašą. ");
            }
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetBorrowRecords(int id)
        {
            var dbContextFactory = new LibraryDbContextFactory();

            try
            {
                using var context = dbContextFactory.CreateDbContext(new string[] { });
                var borrowRecordsRepository = new BorrowRecordRepository(context);

                var borrowRecord = await borrowRecordsRepository.ReadAsync(id);
                if (borrowRecord == null)
                {
                    throw new Exception("Tokio įrašo nėra.");
                }

                return Ok(borrowRecord);
            }
            catch (Exception ex)
            {
                System.IO.File.AppendAllText("Errors.txt", ex.ToString());

                return BadRequest("Serviso klaida.");
            }
            finally
            {
                System.IO.File.AppendAllText("logs.txt", "\r\n" + DateTime.Now.ToString() + $" Atliktas skolinimosi įrašo nr.{id} nuskaitymas. ");
            }
        }

        [HttpGet]
        public async Task<IActionResult> GetBorrowRecords()
        {
            var dbContextFactory = new LibraryDbContextFactory();

            try
            {
                using var context = dbContextFactory.CreateDbContext(new string[] { });
                var borrowRecordsRepository = new BorrowRecordRepository(context);

                var borrowRecords = await borrowRecordsRepository.ReadAsync();
                return Ok(borrowRecords);
            }
            catch (Exception ex)
            {
                System.IO.File.AppendAllText("Errors.txt", ex.ToString());

                return BadRequest("Serviso klaida.");
            }
            finally
            {
                System.IO.File.AppendAllText("logs.txt", "\r\n" + DateTime.Now.ToString() + $" Atliktas visų skolinimosi įrašų nuskaitymas. ");
            }
        }
    }
}
